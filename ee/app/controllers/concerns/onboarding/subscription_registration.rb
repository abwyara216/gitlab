# frozen_string_literal: true

module Onboarding
  class SubscriptionRegistration
    def self.redirect_to_company_form?
      false
    end
  end
end
